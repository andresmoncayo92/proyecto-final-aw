import { useCallback, useContext } from "react";
import Context from "./Context/useContextUsers";

export function useUser(){
  const { token, setToken } = useContext(Context);

  const Login = useCallback(() => {
    setToken('test');
  }, [setToken])

  return{
    isLogged : Boolean(token),
    Login
  };
}