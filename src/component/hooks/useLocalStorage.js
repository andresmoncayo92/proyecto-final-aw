export function useLocalStorage() {

  const obtenerDatosLS = () => {
    let bolsaLS;

    if (localStorage.getItem("__bolsa") === null) {
      bolsaLS = [];
    } else {
      bolsaLS = JSON.parse(localStorage.getItem("__bolsa"));
    }

    return bolsaLS;
  }

  const adminCar = (longLS) => {
    const bolsaCar = document.querySelector(".bolsita.shop-car");
    bolsaCar.querySelector(".producto").innerHTML = longLS;
  }


  const saveProductLS = (productoObject) => {
    let isModific = false;
    let dataLS = obtenerDatosLS();
    dataLS.forEach((item) => {
      if (item.id === productoObject.id) {
        item.quantity = productoObject.quantity;
        item.inforAdd = productoObject.inforAdd;
        isModific = true;
      }
    })

    if (!isModific) {
      dataLS.push(productoObject);
    }
    localStorage.setItem("__bolsa", JSON.stringify(dataLS))
    adminCar(dataLS.length);
  }

  const updateProductLS = (idProduct, cantidad) => {
    let dataLS = obtenerDatosLS();
    dataLS.forEach((item) => {
      if (item.id === idProduct) {
        item.quantity = cantidad;
      }
    })
    localStorage.setItem("__bolsa", JSON.stringify(dataLS));
  }

  const deleteProductLS = (idProduct) => {
    let dataLS = obtenerDatosLS();
    dataLS.forEach((item, index) => {
      if (item.id === idProduct) {
        dataLS.splice(index, 1);
      }
    })
    localStorage.setItem("__bolsa", JSON.stringify(dataLS));
  }

  const calcularValores = () => {
    let productos = obtenerDatosLS();
    let total = 0, subtotales = 0, delivery = 3;
    productos.map(producto => subtotales = subtotales + parseFloat(producto.price * producto.quantity));
    total = parseFloat(subtotales + delivery);
    return {subtotales, total, delivery};
  }

  return [obtenerDatosLS, saveProductLS, updateProductLS, deleteProductLS, calcularValores];
}


