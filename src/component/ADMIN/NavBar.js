export default function NavBar(){
  const user = "Admin";

  return(
    <div className="navbar-align">
        <div className="item-navbar">
            <div className="image">
                <img src="https://i.ytimg.com/vi/XTYk-_seeEE/maxresdefault.jpg" alt=""/>
            </div>
            <p className="name_user">{user}</p>
        </div>
        <div className="item-navbar">
            <button type="button" className="btn-unlogin"><i className="fal fa-sign-out"></i></button>
        </div>
    </div>
  );
}