import { useState } from "react";
import { Link, useLocation, NavLink } from "react-router-dom";
import '../../assets/css/admin/sidebar.css';

import { Routes } from "../../routes";
import Logo from '../../assets/pictures/brand/logo-luchin.svg';
import LogoConcept from '../../assets/pictures/brand/luchin-vino-concept.svg';

export default function SideBar(){
    const location = useLocation();
    const { pathname } = location;
    const [show, setShow] = useState(false);
    const [reducSideBar, setReducSideBar] = useState(false);

    const showClass = show ? "active" : "";
    const logoImage = reducSideBar ? LogoConcept : Logo;

    //reducir tamaño de sideBar
    const onCollapSideBar = () => {
        let divRoot = document.getElementById("root");

        setReducSideBar(!reducSideBar)
        reducSideBar ? divRoot.classList.add('left-navbar') : divRoot.classList.remove('left-navbar');
    };

    const collapseItems = (item) => {
        setShow(!show);
        console.log(item);
    }

    const NavItemsCollapse = (props) => {
        const { icon, title, children = null } = props;

        return(
            <div className="container-item" onClick={(e) => collapseItems(e)}>
                <Link className="item-aside">
                    <i className="estado"></i>
                    <i className={`icon-item ${icon}`}></i>
                    <p className="topic-item">{title}</p>
                    <i className="down-more fas fa-angle-down"></i>
                </Link>
                <div className={`subitem-aside ${showClass}`}>
                    {children}
                </div>
            </div>
        )
    }


    const NavItem = (props) => {
        const { title, icon, path } = props;
        const activeClassName = path === pathname ? "active" : "";

        return(
            <NavLink to={path} className={`item-aside ${activeClassName}`} onClick={()=> setShow(false)}>
                <i className="estado"></i>
                <i className={`icon-item ${icon}`}></i>
                <p className="topic-item">{title}</p>
            </NavLink>
        )
    }


    return(
        <aside className="aside-bar aside">
            <button className="btn__menu" id="button-menu-admin" onClick={onCollapSideBar}><i className="fal fa-bars"></i></button>
            <div className="image-navbar">
                <img src={logoImage} alt="logo-luchin"/>
            </div>
            <div className="legend">PRINCIPAL</div>
            <NavItem title="Resumen" icon="far fa-border-all" path={Routes.Dashboard.path} />
            <NavItemsCollapse title="Menú" icon="far fa-burger-soda">
                <NavItem title="Agregar un Producto" path={Routes.AddProducto.path} />
                <NavItem title="Lista de Menú" path={Routes.ListaMenu.path} />
            </NavItemsCollapse>
            <NavItemsCollapse title="Ordenes" icon="far fa-bags-shopping">
                <NavItem title="Agregar Nueva Orden" path={Routes.AddOrden.path} />
                <NavItem title="Lista de Ordenes" path={Routes.ListaOrdenes.path} />
            </NavItemsCollapse>
            <div className="legend">ADMINISTRACIÓN</div>
            <NavItem title="Clientes" icon="far fa-users" path={Routes.Clientes.path} />
            <NavItem title="Ventas" icon="far fa-analytics" path={Routes.Ventas.path} />
            <NavItem title="Promociones" icon="far fa-badge-percent" path={Routes.PromocionesAdmin.path} />
            <NavItem title="Empleados" icon="far fa-poll-people" path={Routes.Empleados.path} />
            
        </aside>
    )
}