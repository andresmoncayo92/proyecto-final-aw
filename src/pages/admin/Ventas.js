import '../../assets/css/admin/Ventas.css';

export function Ventas() {
    const VentaProducto =[ 
        {
            nombreproducto: 'Hamburguesa de Pollo',
            ingresos: 150,
            cantidad: 50
        },
        {
            nombreproducto: 'Hamburguesa Queso',
            ingresos: 120,
            cantidad: 45
        },
        {
            nombreproducto: 'Alitas BBQ',
            ingresos: 50,
            cantidad: 30
        }
    ]
    const clientes =[ 
        {
            nombre: 'Eddy Mendoza',
            correo: 'eddy@luchin.com',
            telefono: '0987654321',
            pedidos: 50
        },
        {
            nombre: 'Jonathan Espinoza',
            correo: 'jona@luchin.com',
            telefono: '0987654321',
            pedidos: 25
        },
        {
            nombre: 'Andres Lopez',
            correo: 'Landres@luchin.com',
            telefono: '0987654321',
            pedidos: 25
        }   
    ]

    const cards = [
        {
            value : 100,
            legend : "Agosto",
            nameCard: "Ingresos Estimados"
        },
        {
            value : 100,
            legend : "Hamburguesa Rellena",
            nameCard: "Lo más Vendido"
        }
    ]

    return (
        <>
            <div className="theme-apart">
                <h2>Ventas</h2>
            </div>
            <section className="container-main">
                <div className="row-ventas-flex">
                    <div className="cards_sales">
                        {cards.map((card, index) => (
                            <div key={index} class="ctn-card">
                                <p className="cant_cd">{card.value}</p>
                                <p className="tema_cd"> {card.legend}</p>
                                <p className="slogan_cd">{card.nameCard}</p>
                            </div>
                        ))}
                    </div>

                    <div className="tb_cl_orders bd__sect">
                        <h2 className="title_card_cl">Clientes con mayor compras</h2>
                        <div className="ct-table_customers">
                            <table>
                                <thead>
                                    <th>Cliente</th>
                                    <th>Correo Electrónico</th>
                                    <th>Teléfono</th>
                                    <th>Pedidos</th>
                                </thead>
                                <tbody>
                                    {clientes.map(cli=>(
                                        <tr>
                                            <td>{cli.nombre}</td>
                                            <td>{cli.correo}</td>
                                            <td>{cli.telefono}</td>
                                            <td>{cli.pedidos}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div className="row_rk_sales bd__sect">
                    <h2 className="title_card_cl">Ranking de productos vendidos</h2>
                    <div className="ct_sales_tb">
                        <table>
                            <thead>
                                <th>#</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Ingresos Alcanzados</th>
                            </thead>
                            <tbody>
                                {VentaProducto.map(listaP=>(
                                     <tr>
                                        <td>1</td>
                                        <td>{listaP.nombreproducto}</td>
                                        <td>{listaP.cantidad}</td>
                                        <td>${listaP.ingresos}</td>
                                    </tr>

                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

        </>
    );
}

