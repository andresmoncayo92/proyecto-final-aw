import { ErrorMessage, Field, Form, Formik } from "formik";
import { Fragment } from "react";
import '../../assets/css/admin/listamenu.css';
import { Modal } from "../../component/ADMIN/Modal";
import { useModals } from "../../component/hooks/useModals";

export function MenuListProducts()  {
    const [ isOpen, openModal, closeModal ] = useModals();

    const hamburguesa=[
        {
            nombre:"Hamburguesa",
            id:10,
            precio:10
        },
        {
            nombre:"Mendoza",
            id:69,
            precio:1
        },
        {
            nombre:"Sobrina de Mendoza",
            id:4,
            precio:0.50
        },
    ];

    return (  
        <Fragment>
            <Modal 
                topic="Editar Producto del Menú"
                isOpen={isOpen}
                closeModal={closeModal}
            >
                <Formik
                    initialValues={{
                        code: "",
                        textarea: "",
                        file: "",
                        select:"",
                        selectsub:""
                    }}

                    validate={(values)=>{
                        let erroresV = {}

                      /*  if(!values.code){
                                    erroresV.code = "Ingrese el código";
                                }else if(!/([^\w])/g.test(values.code)){
                                    erroresV.code = "El código solo debe contener mayúsculas y números";
                                }
                         if(!values.text){
                                    erroresV.text = "Este campo es requerido";
                                }else if(!/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g.test(values.text)){
                                    erroresV.text = "Este campo no puede contener números ni caracteres especiales";
                                }
                             
                                if(!/(.|)(png)$/g.test(values.file)){
                                    erroresV.file = "Por favor suba un archivo .png";
                                }*/


                               

                        return erroresV;
                    }}

                    onSubmit={(values) => {
                        console.log(values);
                    }}
                >
                    {( {errors} )=>(
                        <Form className="col-form">
                            <div className="grup_field_dash">
                                <p className="row_field_form div">
                                    <label className="labels-dash-sty">Nombre Promoción</label>
                                    <Field
                                        className="input-dash-sty"
                                        type="text"
                                        name="text"
                                        placeholder="Ej: Jueves 2x1"
                                    />
                                    <ErrorMessage name="text" component={() => (<span className="mnj-error alert-dateNacm active">{errors.text}</span>)} />
                                </p>
                                <p className="row_field_form div">
                                    <label className="labels-dash-sty">Código del Cupón</label>
                                    <Field
                                        className="input-dash-sty"
                                        type="code"
                                        name="code"
                                        placeholder="Ej: Ej: JUEV-ESDE-PROM"
                                    />
                                    <ErrorMessage name="code" component={() => (<span className="mnj-error alert-dateNacm active">{errors.code}</span>)} />
                                </p>
                                <p className="row_field_form div">
                                    <label className="labels-dash-sty">Seleccione una Categoria</label>
                                    <Field className="input-dash-sty" name="select" as="select">
                                   
                                        < option value="0">Seleccione Categoria </option>
                                        < option value="1">Hamburguesa </option>
                                        < option value="2">Sanduches </option>
                                        < option value="3">Varios </option>
                                        < option value="4">Bebidas </option>
                                        < option value="5">Cortes </option>
                                        < option value="6">Alitas de Pollo </option>
                                        
                                    </Field>
                                    <ErrorMessage name="select" component={() => (<span className="mnj-error alert-dateNacm active">{errors.select}</span>)} />
                                </p>
                                <p className="row_field_form div">
                                    <label className="labels-dash-sty">Seleccione una SubCategoria</label>
                                    <Field className="input-dash-sty" name="selectsub" as="select">
                                   
                                        < option value="0">Seleccione una SubCategoria </option>
                                        < option value="1">Alcoholicas </option>
                                        < option value="2">No Alcoholicas </option>
                                        < option value="3">Agua </option>
                                        
                                        
                                    </Field>
                                    <ErrorMessage name="select" component={() => (<span className="mnj-error alert-dateNacm active">{errors.selectsub}</span>)} />
                                </p>
                                <p className="row_field_form div">
                                    <label className="labels-dash-sty">Descripcion del <strong>Platillo</strong></label>
                                    <Field
                                        className="textarea-dash-sty"
                                        as="textarea"
                                        name="textarea"
                                        placeholder="Ingresa una Descripcion"
                                    />
                                    <ErrorMessage name="textarea" component={() => (<span className="mnj-error alert-dateNacm active">{errors.textarea}</span>)} />
                                </p>
                                <p className="row_field_form div">
                                    <label className="labels-dash-sty">Seleccione una imagen para el <strong>Platillo</strong></label>
                                    <div class="file_input_text">
                                        <Field
                                            className="file_text"
                                            type="file"
                                            name="file"
                                            placeholder="Ingresa una imagen png"
                                        />
                                        Subir imagen
                                    </div>
                                    <ErrorMessage name="file" component={() => (<span className="mnj-error alert-dateNacm active">{errors.file}</span>)} />
                                </p>
                            </div>
                            
                            <button class="btn-add-data-lista" type="submit">Agregar Platillo</button>
                        </Form>
                    )}
                </Formik>
            </Modal>
            <div className="theme-apart">
                <h2 className="name-user"><span>Lista del Menú</span></h2>
            </div>
            <section className="container-main">
                <div className="grid_producto">
                    <div className="card-cat-plt bd__sect">                    
                        <div className="table-listado">
                            <h2 className="title_card_cl sub">Hamburguesa</h2>
                        </div>
                        <div className="cont_table_card">
                            <table className="table-platillos-card">
                                <thead>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Precio</th>
                                    <th></th>
                                    <th></th>
                                    
                                </thead>
                                <tbody>
                                    {hamburguesa.map(listahamburguesa=>(
                                        <tr>
                                            <td>{listahamburguesa.id}</td>
                                            <td class="detalle-plt">{listahamburguesa.nombre}</td>
                                            <td class="price">${listahamburguesa.precio}</td>

                                            <td>
                                                <button onClick={openModal} class="button-platillo-edit"><i class="far fa-edit"></i></button>
                                            </td>
                                            <td>
                                                <button class="button-platillo-delete"><i class="far fa-trash"></i></button>
                                            </td>
                                        </tr>

                                    ))}
                                </tbody>
                            </table>
                        </div>                    
                    </div>                            
                </div>
            </section>
        </Fragment>
    );
}