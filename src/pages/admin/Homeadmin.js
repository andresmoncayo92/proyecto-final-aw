// import Axios from 'axios'
// import { useEffect, useReducer, useState } from "react";
import { Fragment } from "react";
import '../../assets/css/admin/home.css'

export function Homeadmin() {
    const Usuarioname = {
        nombreUsuario: 'Luis Santana'
    }
    const clientes = {
        Totalclientes: '100'
    }
    const Platillos = {
        Platillosvendidos: '100'
    }
    const Ordenes = {
        Totalordenes: '200',
    }
    const pendorder = [
        {
            pedidID: '100',
            producOrder: 'Hamburguesa Rellena',
            producOrder2: 'Coca-Cola',
            producOrder3: 'Batido',
            priceOrder: '151.95'
        },
        {
            pedidID: '101',
            producOrder: 'Hamburguesa Doble',
            producOrder2: 'Pepsi-Cola',
            producOrder3: 'Jugo de Naranja',
            priceOrder: '25.30'
        },
        {
            pedidID: '102',
            producOrder: 'Hamburguesa Pollo',
            producOrder2: 'Cerveza Club',
            producOrder3: 'Papas Con Cuero',
            priceOrder: '30.30'
        }
    ]
    // var sumador=0;
    // for (var i =0; i <pendorder.length; i++){
        // sumador=sumador+pendorder[i];    
    // };
    // console.log(sumador);
    return (
        <Fragment>
            <div className="theme-apart">
                <h2>Bienvenido, <span className="name-user">{Usuarioname.nombreUsuario}</span></h2>
            </div>
            <section className="container-main">
                <div className="flex-container">
                    <div className="cl1">
                        <div className="group-resume">
                            <div className="card-rsm-yll bd__sect">
                                <div className="dt-resume">
                                    <p className="title">TOTAL DE CLIENTES</p>
                                    <h1 className="total">{clientes.Totalclientes}</h1>
                                    <p className="porcentaje down"><i className="fas fa-arrow-up"></i><span>12.5%</span></p>
                                </div>
                                <div className="icon"><i className="fas fa-users"></i></div>
                            </div>
                            <div className="card-rsm-blue bd__sect">
                                <div className="dt-resume">
                                    <p className="title">PLATOS VENDIDOS</p>
                                    <h1 className="total">{Platillos.Platillosvendidos}</h1>
                                    <p className="porcentaje down"><i className="fas fa-arrow-up"></i><span>12.5%</span></p>
                                </div>
                                <div className="icon"><i className="fas fa-users"></i></div>
                            </div>
                            <div className="card-rsm-pink bd__sect">
                                <div className="dt-resume">
                                    <p className="title">TOTAL DE ORDENES</p>
                                    <h1 className="total">{Ordenes.Totalordenes}</h1>
                                    <p className="porcentaje up"><i className="fas fa-arrow-up"></i><span>12.5%</span></p>
                                </div>
                                <div className="icon"><i className="fas fa-users"></i></div>
                            </div>
                        </div>
                        <div className="table-orders bd__sect">
                            <h2 className="title_card_cl">Ordenes Recientes</h2>
                            <div className="ct-table_orders">
                                <table>
                                    <thead>
                                        <th>Productos</th>
                                        <th>Cliente</th>
                                        <th>Price</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <ul>
                                                    <li>Hamburguesa Relena</li>
                                                    <li>Coca Cola</li>
                                                    <li>Pepsi 1.5 Ltr</li>
                                                </ul>
                                            </td>
                                            <td>Manolo Angulo</td>
                                            <td>$25.50</td>
                                            <td><p className="state-order pendiente">Pendiente</p></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul>
                                                    <li>Hamburguesa Relena</li>
                                                    <li>Coca Cola</li>
                                                    <li>Pepsi 1.5 Ltr</li>
                                                </ul>
                                            </td>
                                            <td>Manolo Angulo</td>
                                            <td>$25.50</td>
                                            <td><p className="state-order entregado">Pendiente</p></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul>
                                                    <li>Hamburguesa Relena</li>
                                                    <li>Coca Cola</li>
                                                    <li>Pepsi 1.5 Ltr</li>
                                                </ul>
                                            </td>
                                            <td>Manolo Angulo</td>
                                            <td>$25.50</td>
                                            <td><p className="state-order en_entrega">Pendiente</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="cl2">
                        <div className="view-order bd__sect">
                            <div className="container-tille__order">
                                <h2 className="title_card_cl">Ordenes Pendientes</h2>
                                <p className="contador">10</p>
                            </div>
                            <div className="container-card_order">
                                {pendorder.map(pendiente=>(<div className="card-order">
                                    <p className="num-order">ID Pedido: <span>{pendiente.pedidID}</span></p>
                                    <div className="dtlle-order_pend">
                                        <ul className="cl-products">
                                            <li>{pendiente.producOrder}</li>
                                            <li>{pendiente.producOrder2}</li>
                                            <li>{pendiente.producOrder3}</li>
                                        </ul>
                                        <p className="cl-price">${pendiente.priceOrder}</p>
                                    </div>
                                </div>))}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}