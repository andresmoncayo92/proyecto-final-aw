import { Link } from 'react-router-dom';
import Logo from '../../assets/pictures/brand/logo-luchin.svg';
import '../../assets/css/admin/components/ModalAuthAdmin.css';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { Routes } from '../../routes';

export function Login() {
  return (
    <div className="main-auth-users">
      <div className="present-slide">
        <div className="overflow"></div>
        <img
          src="https://images.unsplash.com/photo-1505826759037-406b40feb4cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80"
          alt="Dashboard LPL"
        />
      </div>
      <div className="autentif-forms">
        <div className="head-form">
          <div className="brand-logo"><img src={Logo} alt="Logo la parrilla de luchin ventana dashboard" /></div>
          <h3 className="topic-form-aut">¡Bienvenido de nuevo!</h3>
          <p className="instruct-form">Ingrese su dirección de correo electrónico y contraseña para acceder al panel de administración.</p>
        </div>
        <Formik>
          {({ errors }) => (
            <Form className="form-dash-ctn">
              <div className="grup-casilla">
                <p>
                  <label className="labels-dash-sty">Correo Electrónico</label>
                  <Field
                    className="input-dash-sty"
                    type="email"
                    name="email"
                    placeholder="Ingrese su dirección de correo"
                  />
                  <ErrorMessage name="email" component={() => (<span className="mnj-error alert-dateNacm active">{errors.email}</span>)} />
                </p>
                <p>
                  <label className="labels-dash-sty">Contraseña</label>
                  <Field
                    className="input-dash-sty"
                    type="password"
                    name="password"
                    placeholder="Ingrese su contraseña"
                  />
                  <ErrorMessage name="email" component={() => (<span className="mnj-error alert-dateNacm active">{errors.email}</span>)} />
                </p>
              </div>
              <button class="send_form-dash" type="submit">Iniciar Sesión</button>
            </Form>
          )}
        </Formik>
        <div className="more_actionn"><Link to={Routes.ResetPassAdmin.path} className="redirect-page">¿Olvidaste tu Contraseña?</Link></div>
      </div>
    </div>
  );
}