import { Link } from 'react-router-dom';
import Logo from '../../assets/pictures/brand/logo-luchin.svg';
import '../../assets/css/admin/components/ModalAuthAdmin.css';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { Routes } from '../../routes';

export function ResetPassword() {
  return (
    <div className="main-auth-users">
      <div className="present-slide">
        <div className="overflow"></div>
        <img
          src="https://images.unsplash.com/photo-1505826759037-406b40feb4cd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80"
          alt="Dashboard LPL"
        />
      </div>
      <div className="autentif-forms">
        <div className="head-form">
          <div className="brand-logo"><img src={Logo} alt="Logo la parrilla de luchin ventana dashboard" /></div>
          <h3 className="topic-form-aut">Recuperar Contraseña</h3>
          <p className="instruct-form">Ingrese la dirección de correo electrónico con la que inicio sesión anteriormente y le enviaremos un correo electrónico con instrucciones para restablecer su contraseña.</p>
        </div>
        <Formik>
          {({ errors }) => (
            <Form className="form-dash-ctn">
              <div className="">
                <p>
                  <label className="labels-dash-sty">Correo Electrónico</label>
                  <Field
                    className="input-dash-sty"
                    type="email"
                    name="email"
                    placeholder="Ingrese su dirección de correo"
                  />
                  <ErrorMessage name="email" component={() => (<span className="mnj-error alert-dateNacm active">{errors.email}</span>)} />
                </p>
              </div>
              <button class="send_form-dash" type="submit">Restablecer Contraseña</button>
            </Form>
          )}
        </Formik>
        <div className="more_actionn">Prefiero <Link to={Routes.LoginAdmin.path} className="redirect-page">Iniciar Sesión</Link></div>
      </div>
    </div>
  );
}