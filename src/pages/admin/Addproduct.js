import { Fragment } from "react";
import '../../assets/css/admin/Addproduct.css'
export function Addproduct() {
    return (
        <Fragment>
            <div class="theme-apart">
                <h2><span class="name-user">Agregar Producto</span></h2>
            </div>
            <section class="container-main">
                <div class="add-produ_deta dt-resume bd__sect">
                    <h2 class="title_card_cl"> Información Producto</h2>
                    <form action="" class="col-form">
                        <div class="grup_field_dash">
                            <p><label className="labels-dash-sty">Nombre producto</label><input type="text" className="input-dash-sty" placeholder="Ingrese nombre producto" /></p>
                            <p><label className="labels-dash-sty">Producto precio</label><input type="text" className="input-dash-sty" placeholder="Ingrese precio de producto" /></p>
                        </div>
                        <div class="grup_field_dash">
                        <p><label className="labels-dash-sty">Seleccione Categoria</label>
                            <select name="estado_orden" className="select-dash-sty">
                                <option value="0">Seleccione una Categoria</option>
                                <option value="1">Bebidas</option>
                                <option value="2">Hamburguesas</option>
                                <option value="3">Sanduches</option>
                                <option value="4">Alitas</option>
                                <option value="5">Cortes de carne</option>
                                <option value="6">Papas</option>
                            </select></p>
                            <p><label className="labels-dash-sty">Seleccione subcategoria</label>
                            <select name="estado_orden" className="select-dash-sty">
                                <option value="0">Seleccione una subcategoria</option>
                                <option value="1">Bebidas Alcoholicas</option>
                                <option value="2">Bebidas No Alcoholicas</option>
                            </select></p>
                        </div>
                            <div>
                                <p><label className="labels-dash-sty">Imagen promoción</label></p>
                                <div className="file_input_text"> <input type="file" className="file_text" accept="image/gif,image/jpeg,image/jpg,image/png" />Subir imagen</div>
                            </div>
                            <p><label className="labels-dash-sty">descripción de producto</label><textarea className="textarea-dash-sty" placeholder="Hola que tal"></textarea></p>
                        
                        <button class="btn-add-data-produ" type="submit">Agregar producto</button>
                    </form>

                </div>
            </section>
        </Fragment>
    )
}