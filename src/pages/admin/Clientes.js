// import axios from "axios";
// import { Component, useEffect, useState } from "react";
// import { TYPES } from "./actionsCar";

import "../../assets/css/admin/Promociones.css";
import { Modal } from "../../component/ADMIN/Modal";
import { useModals } from "../../component/hooks/useModals";

// export function estadoInicialClient(){
// const [clients, setClients]=useState([]);
// const [estado, setEstado]= useState({
 // clients:[{"cliente_id":1,"nombres":"Andrés Alejandro - Moncayo Zambrano","cliente_telefono":981562798,"cliente_email":"andresmoncayo123@gmail.com","det_orden_cantidad":5}],
//  cart:{}
 //});
// useEffect(()=>{
// axios.get('http://localhost:9000/api/cliente').then(res=>{
//   console.log(res);
// }).catch(error=>{
//   console.log
// })
// })
// }


export function Clientes() {
  const [isOpen, openModal, closeModal] = useModals(false);
  const clientes = [{
    nombre: "Leonel Armando",
    Apellido: "Lucas Anchundia",
    cedula: "1315648759",
    telefono:"0999999999",
    fechaNaci:"2000-08-16",
    correo:"armandounbarquito@gmail.com"
  }]
  return (
    <>
      <Modal
        topic="Detalles Cliente"
        isOpen={isOpen}
        closeModal={closeModal}
        actions={[
          {
            name: "Cerrar",
            type: "cancel",
            className: "btn-modal red",
          },
        ]}>
        {clientes.map(clientes => (
          <div className="grup_field_dash">
            <div>
              <label className="labels-dash-sty">Nombres</label>
              <p className="row_field_form ">{clientes.nombre}</p>
            </div>
            <div>
              <label className="labels-dash-sty">Apellido</label>
              <p className="row_field_form">{clientes.Apellido}</p>
            </div>
            <div>
              <label className="labels-dash-sty">Cedula</label>
              <p className="row_field_form">{clientes.cedula}</p>
            </div>
            <div>
              <label className="labels-dash-sty">Teléfono</label>
              <p className="row_field_form">{clientes.telefono}</p>
            </div>
            <div>
              <label className="labels-dash-sty">Fecha de Nacimiento</label>
              <p className="row_field_form">{clientes.fechaNaci}</p>
            </div>
            <div>
              <label className="labels-dash-sty">Correo electronico</label>
              <p className="row_field_form">{clientes.correo}</p>
            </div>
          </div>
        ))}
      </Modal>
      <div className="theme-apart">
        <h2>Clientes</h2>
        <button
          type="button"
          className="button_th_head"
          onClick={() => openModal()}
        >
          Mostrar Detalles
        </button>

      </div>
    </>
  );
}
