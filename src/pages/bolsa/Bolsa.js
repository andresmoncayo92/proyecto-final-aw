import '../../assets/css/bolsa.css'
import { Routes } from "../../routes";
import { Link } from "react-router-dom";

import { TablaBolsa } from "./tabla";
import { useLocalStorage } from '../../component/hooks/useLocalStorage';
import { useEffect, useState } from 'react';


export function Bolsa()  {
    const [obtenerDatosLS, , updateProductLS, deleteProductLS, calcularValores] = useLocalStorage();
    let productos = obtenerDatosLS();
    let totales  = calcularValores();

    const [ load, setLoad ] = useState(false);
    const [ codeCupon, setCodeCupon ] = useState("");
    let data="";
    

    useEffect(()=>{
        if(load){
            productos = obtenerDatosLS();
            setLoad(false);
            totales = calcularValores();
        }
    }, [productos, load, setLoad]);

    useEffect(()=>{
        setCodeCupon(data);
    }, [data])

    const handleSumCant = (id, value) => {
        value > 0 && value < 10 ? updateProductLS(id, value +1): updateProductLS(id, value); 
        setLoad(true);
    }
    
    const handleResCant = (id, value) => {
        value <= 1 ? updateProductLS(id, value): updateProductLS(id, value - 1);
        setLoad(true);
    }

    const handleChangeCupon = (codigo) => {
        let newElement = codigo;
        newElement = newElement.replace(/([^\w])/g, '');
        setCodeCupon(newElement);        
    }

    const validateC = () => {
        console.log("comprobando")
    }
    
    return (  
        <>
            <div className="textoo pdd_border">
                <h1 className="subtitulo">RESUMEN DE TU BOLSA</h1>
            </div>
            <div className="tabla pdd_border">
                <div>
                    <TablaBolsa productBolsa={productos} sumar={handleSumCant} restar={handleResCant} deleteProd={deleteProductLS} />
                    <div className="cont-totales">
                        <div className="ctn__resumen">
                            <div className="recl-cupon">
                                <div className="topic-cpp">¿Tienes algún cupón? Canjéalo aquí</div>
                                <div className="contenedor-inputt">
                                    <input 
                                        type="text" 
                                        className="cupon-100" 
                                        placeholder="Ingresar el código del cupón" 
                                        onChange={(e)=>{handleChangeCupon(e.target.value.toUpperCase())}} 
                                        value={codeCupon} 
                                        maxLength={12}                                        
                                    />
                                    <button onClick={()=>validateC()} className="inputt-boton">+</button>
                                </div>
                            </div>
                            <div className="sub-iva-total">
                                <p><span className="tema-rsm">SubTotales:</span><span className="price_total_pago">${(totales.subtotales).toFixed(2)}</span></p>
                                <p><span className="tema-rsm">Valor de Envío:</span><span className="price_total_pago">${(totales.delivery).toFixed(2)}</span></p>
                                <p><span className="tema-rsm">Total a Pagar:</span><span id="price_total_pago" className="price_total_pago">${(totales.total).toFixed(2)}</span></p>
                            </div>
                        </div>
                        <div className="cont-botones">
                            <Link className="button-resume-edit" to={Routes.Menu.path}>Ver mas Productos</Link>
                            <Link className="button-resume-delete" to={Routes.ProcessPedido.path}>Procesar Pedido</Link>
                        </div>
                    </div>
                </div>

            </div>
           
        </>
    );
}

