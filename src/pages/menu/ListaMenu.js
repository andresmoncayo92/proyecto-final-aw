import axios from "axios";
import { useEffect, useReducer, useState } from "react";
import { useModals } from "../../component/hooks/useModals";
import { TYPES } from "./actionsCar";
import { carReducer, estadoInicialCar } from "./carReducer";

import { ModalAddCar } from '../../component/components/ModalAddCar';

export function ListaMenu() {
    const [isOpen, openModal, closeModal] = useModals();
    /* const [prod, setProd] = useState([])
    const [estado, setEstado] = useState({
        products:[{ id: 1, title: "Hamburguesa 1", price: 4.99, description: "Soy una descripcion", imageURL: "https://www.ecestaticos.com/file/f369337d113e597004a9d38fa739f2f8/1548864345-color.png", },], 
        cart:{}
    });

    useEffect(() => {
        axios.get('http://localhost:9000/api/menu')
        .then(res => {
            setEstado({
                products: [res.data],
                cart: {}
            });
            setProd(res.data);
        })
        .then(res => res.json())
        .catch(error => {
            console.log(error);
        }) 
        
    }, []); */


    const [state, dispatch] = useReducer(carReducer, estadoInicialCar);
    const { products, cart } = state;

    const AddToBolsa = (id) => {
        dispatch({ type: TYPES.SHOW_DETAILS_PRODUCT, payload: id })
    };

    return (
        <section className="list-product pdd-border">
            <ModalAddCar
                isOpen={isOpen}
                closeModal={closeModal}
                product={Object.entries(cart).length === 0 ? cart.productSelect = {} : cart}
            />
            <div className="grid-product">
                {products.map((producto, index) => (
                    <div key={index} className="card">
                        <div>
                            <div className="img-product">
                                <img src={producto.imageURL} alt={`${producto.title} - La parrilla de Luchin`} />
                            </div>
                            <div className="descripciones">
                                <p className="descrip-product">{producto.title}</p>
                                <p className="precio">${(producto.price).toFixed(2)}</p>
                            </div>
                        </div>
                        <div className="cont-add">
                            <button onClick={() => { AddToBolsa(producto.id); openModal(); }} className="btn-agregar" type="submit">Agregar</button>
                        </div>
                    </div>
                ))}
            </div>
        </section>
    );
}